values = [2, 4, 1, 5]
weight = [10, 3, 2, 5]
sack = 10
pickup_status = [0, 0, 0, 0]

function arr_sum(arr)
    sum = 0
    for i in 1: size(arr, 1)
        sum = sum + arr[i]
    end
    return sum
end

function heuristic(v, w, a, sack_max)
    if arr_sum(w .* a) > sack_max
        return -1
    else
        return arr_sum(v.*a)
    end
end

#generated by swapping items using pickup_status
function neighbours(currentNode)
    neighbours = [[], [], [], []]
    for j in 1: 4
        tmp_node = currentNode
        for i in 1:size(currentNode, 1)
            if(tmp_node[i] == 1)
                tmp_node[i] = 0
            else
                tmp_node[i] = rand((0,1))
            end
        end
        neighbours[j] = tmp_node 
    end
    return neighbours
end

function eval(i)
    return heuristic(values, weight, i, 10)
end

#hill hill_climbing function
function hill_climb(startNode)
    currentNode = startNode
    while true
        L = neighbours(currentNode)
        nextEval = -10000
        nextNode = nothing
        for i in 1: size(L, 1)
            if (eval(L[i]) > nextEval)
                nextNode = L[i]
                nextEval = eval(L[i])
            end
        end

        if nextEval <= eval(currentNode)
            return currentNode
        end

        if(nextEval > -1 )
            currentNode = nextNode
        end
    end

end
println(hill_climb([0, 1, 0, 0]))
