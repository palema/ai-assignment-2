function argmax(seq::T, fn::Function) where {T <: Vector}
    local best_element = seq[1];
    local best_score = fn(best_element);
    for element in seq
        element_score = fn(element);
        if (element_score > best_score)
            best_element = element;
            best_score = element_score;
        end
    end
    return best_element;
end

function if_(boolean_expression::Bool, ans1::Any, ans2::Any)
    if (boolean_expression)
        return ans1;
    else
        return ans2;
    end
end


abstract type AbstractGame end;

#=
    User defined game, 
=#
struct Game <: AbstractGame
    initial::String
    nodes::Dict
    utilities::Dict

    function Game()
        return new("A", Dict([
            Pair("A", Dict("A1"=>"B",  "A2"=>"C",  "A3"=>"D")),
            Pair("B", Dict("B1"=>"B1", "B2"=>"B2", "B3"=>"B3")),
            Pair("C", Dict("C1"=>"C1", "C2"=>"C2", "C3"=>"C3")),
            Pair("D", Dict("D1"=>"D1", "D2"=>"D2", "D3"=>"D3")),
            ]), Dict([
                Pair("B1", 3),
                Pair("B2", 12),
                Pair("B3", 8),
                Pair("C1", 2),
                Pair("C2", 4),
                Pair("C3", 6),
                Pair("D1", 14),
                Pair("D2", 5),
                Pair("D3", 2),
                ]));
    end
end

#=
  returns valid actions a player can perform  
=#
function actions(game::Game, state::String)
    return collect(keys(get(game.nodes, state, Dict())));
end

function result(game::Game, state::String, move::String)
    return game.nodes[state][move];
end

#= 
    returns utility at a given node
=#
function utility(game::Game, state::String, player::String)
    if (player == "MAX")
        return game.utilities[state];
    else
        return -game.utilities[state];
    end
end

#= 
    test to check if at end node
=#
function terminal_test(game::Game, state::String)
    return !(state in ["A", "B", "C", "D"]);
end

#=
    returns which players turn to make a move
=#
function to_move(game::Game, state::String)
    return if_((state in ["B", "C", "D"]), "MIN", "MAX");
end


function max_value(game::T, player::String, cutoff_test_fn::Function, evaluation_fn::Function, state::String, alpha::Number, beta::Number, depth::Int64) where {T <: AbstractGame}
    if (cutoff_test_fn(state, depth))
        return evaluation_fn(state);
    end
    local v::Float64 = -Inf64;
    for action in actions(game, state)
        v = max(v, min_value(game, player, cutoff_test_fn, evaluation_fn, result(game, state, action), alpha, beta, depth + 1));
        if (v >= beta)
            return v;
        end
        alpha = max(alpha, v);
    end
    return v;
end

function min_value(game::T, player::String, cutoff_test_fn::Function, evaluation_fn::Function, state::String, alpha::Number, beta::Number, depth::Int64) where {T <: AbstractGame}
    if (cutoff_test_fn(state, depth))
        return evaluation_fn(state);
    end
    local v::Float64 = Inf64;
    for action in actions(game, state)
        v = min(v, max_value(game, player, cutoff_test_fn, evaluation_fn, result(game, state, action), alpha, beta, depth + 1));
        if (v >= alpha)
            return v;
        end
        beta = min(alpha, v);
    end
    return v;
end

#=
    minimax recursive function
=#
function minimax(state::String, game::T; d::Int64=4, cutoff_test_fn::Union{Nothing, Function}=nothing, evaluation_fn::Union{Nothing, Function}=nothing) where {T <: AbstractGame}
    local player::String = to_move(game, state);
    if (typeof(cutoff_test_fn) <: Nothing)
        cutoff_test_fn = (function(state::String, depth::Int64; dvar::Int64=d, relevant_game::AbstractGame=game)
                            return ((depth > dvar) || terminal_test(relevant_game, state));
                        end);
    end
    if (typeof(evaluation_fn) <: Nothing)
        evaluation_fn = (function(state::String, ; relevant_game::AbstractGame=game, relevant_player::String=player)
                            return utility(relevant_game, state, relevant_player);
                        end);
    end
    return argmax(actions(game, state),
                    (function(action::String,; relevant_game::AbstractGame=game, relevant_state::String=state, relevant_player::String=player, cutoff_test::Function=cutoff_test_fn, eval_fn::Function=evaluation_fn)
                        return min_value(relevant_game, relevant_player, cutoff_test, eval_fn, result(relevant_game, relevant_state, action), -Inf64, Inf64, 0);
                    end));
end


function player(game::T, state::String) where {T <: AbstractGame}
    return minimax(state, game);
end

#=
    function starts game
=#
function play_game(game::T, players::Vararg{Function}) where {T <: AbstractGame}
    state = game.initial;
    while (true)
        for player in players
            move = player(game, state);
            state = result(game, state, move);
            if (terminal_test(game, state))
                return utility(game, state, to_move(game, game.initial));
            end
        end
    end
end


game = Game()
player1 = player
player2 = player
println(play_game(game, player1, player2))
